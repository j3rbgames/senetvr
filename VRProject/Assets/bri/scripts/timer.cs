﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    public Text tempsText;
    public float temps = 0.0f;
    private bool estaPausat = false;
    public Image obe1, obe2, obe3;
    public Animation sandclock;

    private void Start()
    {

        obe1.color = new Color(obe1.color.r, obe1.color.g, obe1.color.b, 0.5f);
        obe2.color = new Color(obe2.color.r, obe2.color.g, obe2.color.b, 0.5f);
        obe3.color = new Color(obe3.color.r, obe3.color.g, obe3.color.b, 0.5f);


    }

    // Update is called once per frame
    void Update()
    {
            
        if (estaPausat == false)
        {
            temps -= Time.deltaTime;
            tempsText.text = "" + temps.ToString("f0");
            if(temps <= 50)
            {
                obe1.color = new Color(obe1.color.r, obe1.color.g, obe1.color.b, 1f);

            }
            if (temps <= 0)
            {
                pausa();
            }
            Time.timeScale = 1f;
            sandclock["sandclock"].speed = 1;
            
        }
        if (estaPausat == true)
        {
            Time.timeScale = 0f;
            sandclock["sandclock"].speed = 0;
        }

        
        
    }

    private void pausa()
    {
        estaPausat = true;
    }

    private void continuar()
    {
        estaPausat = false;
    }
}
